import pandas
import os
from xlrd.biffh import XLRDError

class ExcelFileManager:
    """
    Excelファイル操作機能
    """
    def __init__(self, filepath):
        self.initialize(filepath)


    def initialize(self, filepath):
        """ 初期化処理 """
        self._book = None
        self._sheets = {}
        self.load_book(filepath)
        self.load_sheets()


    def load_book(self, filepath):
        """ ブック読み込み """
        filename = filepath.rsplit('/')[-1]
        filedir = filepath.replace(filename, '')
        
        # 作業ディレクトリを移動
        os.chdir(filedir)

        try:
            self._book = pandas.ExcelFile(filename)
            return self
        except XLRDError as e:
            # エクセルファイルの読み込みエラー
            print(e)
            #sheet = file.parse('test', header=None)
            #row = sheet.iloc[0]
            #print(sheet)
            #return str(sheet)


    def load_sheets(self):
        """ シート読み込み """
        if self._book is None:
            print('err@load_sheets')
            return self

        sheet_names = self._book.sheet_names
        print(sheet_names)
        for name in sheet_names:
            self._sheets[name] = self._book.parse(name, header=None)

        return self
    

    def get_sheet(self, sheet_name):
        return self._sheets[sheet_name]
    

    def debug(self):
        #for row in self.sheets.iterrows():
        #    print(row)
        print(self._sheets)
        return self


class PhpFileManager:
    """
    phpファイル操作機能
    """
    def __init__(self):
        pass

    def write(self, filename, code):
        with open('output/' + filename,'w') as f:
            f.write(code)


if __name__ == '__main__':
    pass



