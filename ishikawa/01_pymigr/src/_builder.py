import re
from _migration_const import ColumnConst as COL_C
from abc import ABCMeta, abstractmethod

class ABuilder(metaclass=ABCMeta):
    """
    Builder基底クラス
    """
    def __init__(self):
        self._children = []
        self._code = ''
 
 
    def _set_head(self, head_val):
        """ ヘッダの設定 """
        self._head = head_val
        return self
    

    def _set_foot(self, foot_val):
        """ フッタの設定 """
        self._foot = foot_val
        return self


    def _wrap(self):
        """ ヘッダ/フッタを適用 """
        self._code = self._head + self._code + self._foot
        return self


    def add_child(self, builder):
        """ 入れ子にするコードを追加 """
        self._children.append(builder._code)
        return self


    def _indent(self):
        """ 半角スペース(4つ)を付与してインデント """
        H_SPACE = '    '
        self._children = [child.replace('\n', '\n' + H_SPACE) for child in self._children]
        return self


    def _apply_children(self):
        """ 入れ子のコードを反映 """
        for child in self._children:
            self._code += child
        self._code += '\n'
        return self


    @abstractmethod
    def _initialize(self):
        """ 初期化 """
        pass

    @abstractmethod
    def build(self):
        """ ソースコード生成 """
        pass


class ColumnBuilder(ABuilder):
    """
    カラム定義生成Builder
    """
    def __init__(self):
        super().__init__()

        self._set_head('\n$table->')\
            ._set_foot(';')\
            ._initialize()

    def _initialize(self):
        """ 初期化処理 """
        # メンバの初期化
        self._name = ''
        self._type = ''
        self._nullable = ''
        self._description = ''

        # 型とスキーマビルダのマッピング
        self.type_map = {
            'boolean': COL_C.BOOLEAN,
            'date': COL_C.DATE,
            'datetime':COL_C.DATE_TIME,
            'decimal': COL_C.DECIMAL,
            'int': COL_C.INTEGER,
            'text': COL_C.TEXT,
            'time': COL_C.TIME,
            'varchar': COL_C.STRING,
        }


    def set_name(self, name_val):
        """ カラム名の設定 """
        self._name = name_val
        return self
    

    def set_type(self, type_val):
        """ 型の設定 """
        self._type = self._parse_type(type_val)
        return self


    def set_nullable(self, null_val):
        """ NOT NULL制約の設定 """
        if null_val != 'false':
            self._nullable = 'nullable()'
        else:
            self._nullable = ''
        return self


    def set_description(self, value):
        """ カラムコメントの設定 """
        self._description = value
        return self
    

    def build(self):
        """ ソースコード生成 """
        return self._append(self._type)\
            ._append(self._nullable)\
            ._append(self._description)\
            ._wrap()


    def _parse_type(self, type_val):
        """ 型のパース """
        if not '(' in type_val:
            # 桁数の指定が無い型
            method = self.type_map[type_val].value
            return method + '(\'' + self._name + '\')' 
        
        # 括弧で分割
        params = re.split('[(,)]', type_val)

        method = self.type_map[params[0]].value
        arguments = str(params[1]) if len(params) == 3 \
                    else str(params[1]) + ', ' + str(params[2])

        return method + '(\'' + self._name + '\', ' + arguments + ')'


    def _append(self, code):
        """ コードへメンバ変数を追加 """
        if (code == ''):
            return self
        
        # 中間のコードだけに'->'を付与する
        isFirstOrLast = self._code == '' or code == self._foot

        self._code += (code if isFirstOrLast else ('->' + code))
        return self


class CreateTableBuilder(ABuilder):
    """
    テーブル作成処理生成Builder
    """
    def __init__(self, table_name):
        super().__init__()

        self._set_head(f'\nSchema::create(\'{table_name}\', function (Blueprint $table) {{')\
            ._set_foot('});')\
            ._initialize()

    def _initialize(self):
        """ 初期化処理 """
        pass


    def build(self):
        """ ソースコード生成 """
        return self._indent()\
                ._apply_children()\
                ._wrap()


class DropTableBuilder(ABuilder):
    """
    テーブル削除処理生成Builder
    """
    def __init__(self, table_name):
        super().__init__()

        self._set_head(f'\nSchema::dropIfExists(\'{table_name}\'')\
            ._set_foot(');')\
            ._initialize()

    def _initialize(self):
        """ 初期化処理 """
        pass


    def build(self):
        """ ソースコード生成 """
        return self._indent()\
                ._wrap()


class FunctionBuilder(ABuilder):
    """
    ファンクション生成Builder
    """
    def __init__(self, method_name):
        """
        コンストラクタ

        Parameters
        ----------
        method_name : string
            "up" or "down" を指定
        """
        super().__init__()
        
        if method_name not in ['up', 'down']:
            print('invalid method type')
            # TODO: raise exception
            pass        

        self._set_head(f'\npublic function {method_name}()\n{{')\
            ._set_foot('}\n')\
            ._initialize()


    def _initialize(self):
        """ 初期化処理 """
        pass


    def build(self):
        """ ソースコード生成 """
        return self._indent()\
                ._apply_children()\
                ._wrap()


class ClassBuilder(ABuilder):
    """ クラス生成Builder """
    def __init__(self, table_name):
        super().__init__()

        table_name_upper = self._capitalize_table_name(table_name)
        
        head = '<?php\n\n' +\
                'use Illuminate\Support\Facades\Schema;\n' +\
                'use Illuminate\Database\Schema\Blueprint;\n' +\
                'use Illuminate\Database\Migrations\Migration;\n' +\
                f'\nclass Create{table_name_upper}Table extends Migration\n{{'

        self._set_head(head)\
            ._set_foot('}')\
            ._initialize()

    def _initialize(self):
        """ 初期化処理 """
        pass


    def build(self):
        """ ソースコード生成 """
        return self._indent()\
                ._apply_children()\
                ._wrap()


    def _capitalize_table_name(self, table_name):
        """ テーブル名をスネークケースからキャメルケースへ変換 """
        splited_name = table_name.split('_')
        return ''.join([name.capitalize() for name in splited_name])

#
# DEBUG
# =====
if __name__ == '__main__':
    #print(ColumnBuilder().set_name('decimal_test').set_type('decimal(11,2)').build()._code)
    #print(ColumnBuilder().set_name('varchar_test').set_type('varchar(255)').build()._code)
    #print(ColumnBuilder().set_name('text_test').set_type('text').build()._code)

    clild_1 = ColumnBuilder().set_name('decimal_test').set_type('decimal(11,2)').build()
    table_1 = CreateTableBuilder('test_table').add_child(clild_1).build()
    d_table_1 = DropTableBuilder('test_table').build()
    func_1 = FunctionBuilder('up').add_child(table_1).build()
    func_2 = FunctionBuilder('down').add_child(d_table_1).build()

    #print(func_1._code)
    #print(func_2._code)

    cls_1 = ClassBuilder('test_table').add_child(func_1).add_child(func_2).build()
    print(cls_1._children)
    print(cls_1._code)

