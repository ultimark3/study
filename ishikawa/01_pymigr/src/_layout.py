import PySimpleGUI as sg
from _manager import ExcelFileManager as xlmng, PhpFileManager as phmng
from _builder import *
layout = [      
            [sg.Text('Filename')],
            [sg.Input(key='file_select', enable_events=True, disabled=True), sg.FileBrowse()],
            [sg.Listbox(key='sheet_names', values=[], size=(30, 6))],
            [sg.Submit( key='run', button_text='実行')]      
]

window = sg.Window('pymigr', layout)         

while True:      
    event, values = window.Read()      
    
    if event == None:  
        print('none')
        break      

    if event == 'file_select':
        filepath = values['file_select']

        # エクセルファイル読み込み
        xl_manager = xlmng(filepath)
        sheet_names = list(xl_manager._sheets) # keys()はインデックスでアクセス不可のためlist()

        print('file_select')
        window.element('sheet_names').Update(sheet_names)

    if event == 'run':
        # PySimpleGUIには選択中の値を取得するAPIが無い？
        # 暫定対応(https://github.com/PySimpleGUI/PySimpleGUI/issues/1633)
        # 選択中のindexを取得
        selected_index = window.element('sheet_names').Widget.curselection()[0]
        selected_value = sheet_names[selected_index]

        target_sheet = xl_manager.get_sheet(selected_value)

        # ソースコード生成
        c_table_builder = CreateTableBuilder(selected_value)
        d_table_builder = DropTableBuilder(selected_value).build()

        for i in range(4, len(target_sheet)):
            row = target_sheet.iloc[i]
            print(row)
            # NOTE: DataFrame
            # TODO: 行から列を抽出してBuilderへ受け渡す
            # TODO: NaNは空値へ変換する
        """
            # 各行からカラム定義を生成
            column_builder = ColumnBuilder()\
                                .set_name(row[:1])\
                                .set_type(row[:2])\
                                .set_nullable(row[:3])\
                                .set_description(row[:4])\
                                .build()

            # カラム定義をテーブル定義へ追加
            c_table_builder.add_child(column_builder)
        
        c_table_builder.build()

        up_func_builder = FunctionBuilder('up')\
                            .add_child(c_table_builder)\
                            .build()

        down_func_builder = FunctionBuilder('down')\
                                .add_child(d_table_builder)\
                                .build() 

        class_builder = ClassBuilder(selected_value)\
                            .add_child(up_func_builder)\
                            .add_child(down_func_builder)\
                            .build()

        print(class_builder._code)
        # phmng().write('test.php', test_str)
        """
            