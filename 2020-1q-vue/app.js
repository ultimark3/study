new Vue({
    el: '#app',
    data: {
        newItem: '',
        items: [
            {title: 'あいうえお', isChecked: false },
            {title: 'かきくけこ', isChecked: false },
            {title: 'さしすせそ', isChecked: false },
        ]
    },
    mounted() { // ローカルストレージからの取得
        if (localStorage.getItem('items')) {
            try {
                this.items = JSON.parse(localStorage.getItem('items')); // json形式でitems配列の値を取得
            } catch(e) {
                localStorage.removeItem('items'); // 問題発生の場合はデータが破損していると判断し削除
            }
        }
    },
    methods:{
        hello: function() {
            return this.message + "@Hoge";
        },
        addTodo: function() {
            this.items.push({
                title: this.newItem,
                isChecked: false
            });
            this.newItem = '';
            this.saveTodo(); // ローカルストレージへの保存
        },
        deleteTodo: function(item) {
            let index = this.items.indexOf(item);
            this.items.splice(index, 1);
            this.saveTodo(); // ローカルストレージへ、削除したことの保存
        },
        saveTodo() { // ローカルストレージへの保存
            const parsed = JSON.stringify(this.items);
            localStorage.setItem('items', parsed);
        }
    }
})