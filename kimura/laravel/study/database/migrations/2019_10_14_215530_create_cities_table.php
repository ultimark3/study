<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 都市テーブル作成マイグレーションクラス
 *
 * 都市テーブル作成
 * 作成コマンド
 * $ php artisan make:migration create_cities_table
 * 実行コマンド
 * $ php artisan migrate --seed
 *
 * @package Migration
 */
class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->comment('都市名');
            $table->string('image', 100)->nullable()->comment('画像');
            $table->string('thumbnail', 100)->nullable()->comment('サムネイル画像');
            $table->unsignedBigInteger( 'country_id' )->comment('国ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
