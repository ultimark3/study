<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 国テーブル作成マイグレーションクラス
 *
 * 国テーブル作成
 * 作成コマンド
 * $ php artisan make:migration create_countries_table
 * 実行コマンド
 * $ php artisan migrate --seed
 *
 * @package Migration
 */
class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->comment('国名');
            $table->string('currency', 100)->comment('通貨');
            $table->decimal('rate', 8, 2)->comment('レート');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
