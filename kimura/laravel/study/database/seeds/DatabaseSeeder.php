<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // ここにシーダークラスを宣言すると登録される。
        $this->call(CountryTableSeeder::class);
        $this->call(CityTableSeeder::class);
    }
}
