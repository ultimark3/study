<?php

use Illuminate\Database\Seeder;

/**
 * 都市テーブルシーダークラス
 *
 * 都市テーブル作成
 * 作成コマンド
 * $ php artisan make:seeder CityTableSeeder
 * 実行コマンド
 * $ php artisan migrate --seed
 *
 * @category Seeder
 * @package Seeder
 */
class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [   // ニューヨーク
                'id' => 1,
                'name' => 'ニューヨーク',
                'country_id' => 1,
            ],
        ]);
    }
}
