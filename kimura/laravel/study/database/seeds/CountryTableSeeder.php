<?php

use Illuminate\Database\Seeder;

/**
 * 国テーブルシーダークラス
 *
 * 国テーブル作成
 * 作成コマンド
 * $ php artisan make:seeder CountryTableSeeder
 * 実行コマンド
 * $ php artisan migrate --seed
 *
 * @category Seeder
 * @package Seeder
 */
class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [   // アメリカ
                'id' => 1,
                'name' => 'アメリカ',
                'currency' => 'USD',
                'rate' => 107.37,
            ],
        ]);
    }
}
