@extends('layouts.back')

@section('title', '国を追加')

@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>国管理</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="./">国管理</a></li>
                        <li class="active">国管理</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <a href="{{ url('admin/country/input') }}" class="btn btn-primary btn-sm text-white">国を追加</a>
                </div>
                <div class="card">
                    <table class="table">
                        <thead class="bg-light">
                        <tr>
                            <th style="width:15%" scope="col">国名</th>
                            <th style="width:10%" scope="col">通貨</th>
                            <th style="width:15%" scope="col">レート</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="country-id.php" class="text-primary">タイ</a></td>
                            <td>THB</td>
                            <td>+9</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--/.col-->

        </div><!--/.row-->

    </div> <!-- .content -->

@endsection
