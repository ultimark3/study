@extends('layouts.back')

@section('title', '国を追加')

@section('content')

    <div class="content mt-3">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table mb-0">
                            <tbody>
                            <tr>
                                <th style="width: 30%">ステータス</th>
                                <td>
                                    <div class="form-check row">
                                        <div class="radio col-3">
                                            <label for="radio1" class="form-check-label">
                                                <input checked="" type="radio" id="radio1" name="radios-tou"
                                                       class="form-check-input">表示
                                            </label>
                                        </div>
                                        <div class="radio col-3">
                                            <label for="radio2" class="form-check-label">
                                                <input type="radio" id="radio2" name="radios-tou" value="option2"
                                                       class="form-check-input">非表示
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" colspan="1" style="width: 30%">国名</th>
                                <td colspan=""><input style="width: 200px" type="text" class="form-control" value="イギリス">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" colspan="1">通貨</th>
                                <td colspan="">
                                    <input style="width: 200px" type="text" class="form-control d-inline-block"
                                           value="EUR">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" colspan="1">レート</th>
                                <td colspan="">
                                    <div style="width: 200px"
                                         class="d-flex justify-content-between align-items-center pb-2">
                                        <input type="text" class="form-control d-inline-block" style="width: 150px"
                                               value="0.29">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 25%">
                                    都市
                                </th>
                                <td>
                                    <div class="border-bottom pb-3">
                                        <div>
                                            <input type="text" class="form-control d-inline mb-3" style="width: 200px"
                                                   value="イギリス">
                                             <label>非表示 <input type="checkbox" value=""></label>
                                            <button type="button" class="close" aria-label="閉じる">
                                                　<span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div>
                                            <img src="images/country-bangkok.png" alt="リンク画像">
                                            <button type="button" class="close float-none d-inline-block ml-3"
                                                    aria-label="閉じる" style="">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="border-bottom pb-3 mt-3">
                                        <input type="text" class="form-control d-inline mb-3" style="width: 200px"
                                               value="ロンドン">
                                         <label>非表示 <input type="checkbox" value=""></label>
                                        <button type="button" class="close" aria-label="閉じる">
                                            　<span aria-hidden="true">×</span>
                                        </button>
                                        <div>
                                            <img src="images/country-chenmai.png" alt="リンク画像">
                                            <button type="button" class="close text-left float-none d-inline-block ml-3"
                                                    aria-label="閉じる" style="">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-secondary mt-3">追加する</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 text-center mb-5">
                    <a href="countries.php">
                        <button class="btn btn-success btn-lg mr-3">保存</button>
                    </a>
                </div>

            </div>
        </div>
    </div>


@endsection
