<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// 管理画面
// ダッシュボード画面
Route::get('/admin', 'Back\DashboardController@index');
// 国一覧画面
Route::get('/admin/country', 'Back\CountryController@index');
// 国登録画面
Route::get('/admin/country', 'Back\CountryController@input');
