<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * 国管理画面登録クラス
 * 
 * 以下の機能を有する
 * ・一覧表示
 * ・登録・更新
 * 
 * 作成コマンド
 * $ php artisan make:controller Back/CountryController
 * 
 * @category Controller
 * @package App\Http\Controllers\Back
 */
class CountryController extends Controller
{
    /**
     * 一覧画面
     * URL
     * http://localhost:8888/admin/country
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('back.country.index');
    }

    /**
     * 入力画面
     * URL
     * http://localhost:8888/admin/country/input
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function input()
    {
        return view('back.country.input');
    }
}
