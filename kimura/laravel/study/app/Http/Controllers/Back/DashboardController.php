<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * ダッシュボードコントローラークラス
 *
 * ダッシュボードコントローラー
 *
 * 作成コマンド
 * $ php artisan make:controller Back/DashboardController
 *
 * @category Controller
 * @package App\Http\Controllers\Back
 */
class DashboardController extends Controller
{
    /**
     * ダッシュボード画面
     * URL
     * http://localhost:8888/admin
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('back.dashboard');
    }
}
