<?php

namespace App\Repositories;

use App\Models\Country;
use Illuminate\Support\Facades\DB;

/**
 * 国リポジトリクラス
 *
 * @category RepositoryInterface
 * @package App\Repositories
 */
class CountryRepositoryInterface implements CountryRepositoryInterface
{
    /**
     * 一覧データ取得
     *
     * @return Country 国データ一覧
     */
    public function getAll()
    {
        return DB::table('countries')
        ->get();
    }

    /**
     * テーブルへ登録
     *
     * @param int $owner_id
     * @param string $lang
     * @param int $level
     */
    public function store(int $owner_id, string $lang, int $level)
    {

    }
}
