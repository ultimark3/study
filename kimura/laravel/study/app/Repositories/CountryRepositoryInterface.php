<?php

namespace App\Repositories;

use App\Models\Country;

/**
 * 国リポジトリインターフェース
 * 
 * - getAll 一覧取得
 *
 * @category RepositoryInterface
 * @package App\Repositories
 */
interface CountryRepositoryInterface
{
    /**
     * 一覧データ取得
     *
     * @return Country 国データ一覧
     */
    public function getAll();

    /**
     * テーブルへ登録
     *
     * @param int $owner_id
     * @param string $lang
     * @param int $level
     */
    public function store(int $owner_id, string $lang, int $level);
}
