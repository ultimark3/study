<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 *
 * 作成コマンド
 * $ php artisan make:model Models/Country
 *
 * @package App\Models
 */
class City extends Model
{
    /**
     * city->countryでCountryのレコードを取得できるようにする。
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
