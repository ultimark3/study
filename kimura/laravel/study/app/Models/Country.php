<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * 作成コマンド
 * $ php artisan make:model Models/Country
 *
 * @package App\Models
 */
class Country extends Model
{
    /**
     * country->cityでCityのレコードを取得できるようにする。
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    Public function City()
    {
        return $this->hasMany('App\Models\City');
    }
}
