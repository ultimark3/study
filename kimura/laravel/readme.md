# 作業メモ

## 初回起動

- Dockerを起動する
~~~
$ cd docker
$ docker-compose up -d
~~~

- Docker起動後にDockerサーバに入る
~~~
$ docker exec -it app /bin/bash
~~~

- ComposerでLaravelをインストールする
~~~
$ composer update
~~~

- マイグレーションでテーブルを作る

~~~
$ php artisan migrate:refresh --seed
~~~

- 起動確認

  - Laravel管理画面
    - http://localhost:800/admin
  - PhpMyAdmin
    - http://localhost:900/

## migration

### テーブル作成

php artisan make:migration create_countries_table
php artisan make:migration create_cities_table

### Seeder

php artisan make:seeder CountryTableSeeder
php artisan make:seeder CityTableSeeder
